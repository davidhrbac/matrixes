while read p; do
  echo "Downloading $p"
  julia getmatrix.lj $p > /dev/null 2>&1
  FILENAME=$(basename $p)
  MMFILE=$JULIA_DEPOT_PATH/packages/MatrixDepot/WrlBf/data/uf/$p/$FILENAME.mtx
  ls -al $MMFILE 
  python mm2Petsc.py $MMFILE
done < <(grep -v '^\s*$\|^\s*\#' matrixes.txt)

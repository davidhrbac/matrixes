export GIT_AUTHOR_NAME="David Hrbáč"
export GIT_AUTHOR_EMAIL=david@hrbac.cz
export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"
source /etc/bash_completion.d/git

export JULIA_DEPOT_PATH="/mnt/storage_1/scratch/scratch/pr2c0046/julia"
export PATH=~/hrbac/julia-1.5.3/bin/:$PATH
export PETSC_DIR=/mnt/storage_1/scratch/scratch/pr2c0046/petsc/
export PETSC_ARCH=intel-opt
module load python
